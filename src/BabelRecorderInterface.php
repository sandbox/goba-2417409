<?php

/**
 * @file
 * Contains \Drupal\babel\BabelRecorderInterface.
 */

namespace Drupal\babel;

interface BabelRecorderInterface {

  /**
   * Returns data recorded for this type of translation service.
   *
   * @return array
   *   An array keyed by language code and then keyed as appropriate for the
   *   data collected by the recorder service.
   */
  function getRecordedData();

}