<?php

/**
 * @file
 * Contains \Drupal\Core\Installer\InstallerServiceProvider.
 */

namespace Drupal\babel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Modifies the configuration language override service.
 */
class BabelServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $override_service = $container->getDefinition('language.config_factory_override');
    $override_service->setClass('Drupal\babel\ConfigTranslationRecorder');
  }

}
