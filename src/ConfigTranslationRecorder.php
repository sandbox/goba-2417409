<?php

/**
 * @file
 * Contains \Drupal\babel\ConfigTranslationRecorder.
 */

namespace Drupal\babel;

use Drupal\language\Config\LanguageConfigFactoryOverride;

/**
 * Provides language overrides and records requests to them.
 */
class ConfigTranslationRecorder extends LanguageConfigFactoryOverride implements BabelRecorderInterface {

  /**
   * Caches lookups for translated configuration.
   *
   * @var array
   */
  protected $lookups = array();

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    if ($this->language) {
      foreach ($names as $name) {
        $this->getOverride($this->language->getId(), $name);
      }
    }
    return parent::loadOverrides($names);
  }

  /**
   * {@inheritdoc}
   */
  public function getOverride($langcode, $name) {
    $override = parent::getOverride($langcode, $name);
    $this->lookups[$langcode][$name] = (bool) $override->get();
    return $override;
  }

  /**
   * @inheritdoc
   *
   * @return array
   *   List of language codes associated with a list of booleans keyed by
   *   configuration names. The boolean is TRUE if the override contained
   *   data, FALSE otherwise.
   */
  public function getRecordedData() {
    return $this->lookups;
  }

}
