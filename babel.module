<?php

/**
 * @file
 * Module providing insights about the multilingual behavior of the site.
 */

use Drupal\babel\BabelRecorderInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\config_translation\ConfigEntityMapper;

/**
 * Implements hook_language_fallback_candidates_alter().
 */
function babel_language_fallback_candidates_alter(array &$candidates, array $context, $get = NULL) {
  static $entities = array();

  if (isset($get)) {
    return $entities;
  }

  if ($context['operation'] == 'entity_view' && $context['data'] instanceof EntityInterface) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $context['data'];
    $entities[$context['langcode']][$entity->getEntityTypeId()][$entity->id()] = $entity->hasTranslation($context['langcode']);
  }
}

/**
 * Implements hook_preprocess_HOOK() for page templates.
 */
function babel_preprocess_page(&$variables) {
  // Handle recorded interface translation data.
  $interface_list = array(
    '#theme' => 'item_list',
    '#items' => array(),
    '#title' => t('Interface'),
  );
  /** @var \Drupal\babel\InterfaceTranslationRecorder $interface_recorder */
  $interface_recorder = \Drupal::service('string_translator.babel');
  $strings = $interface_recorder->getRecordedData();
  if (count($strings)) {
    foreach($strings as $langcode => $contexts) {
      foreach ($contexts as $context => $string_list) {
        foreach (array_keys($string_list) as $string) {
          /** @var \Drupal\locale\StringDatabaseStorage $translation */
          $translation = \Drupal::service('locale.storage')->findTranslation(array(
            'language' => $langcode,
            'source' => $string,
            'context' => $context,
          ));
          if ($translation && !empty($translation->translation)) {
            $class = 'babel--translated';
          }
          else {
            $class = 'babel--untranslated';
          }

          $t_vars = array(
            '%langcode' => $langcode,
            '@string' => $string,
            '@url' => \Drupal::url('locale.translate_page', array(), array('query' => array('langcode' => $langcode, 'string' => $string))),
            '@context' => $context,
          );
          if (!empty($context)) {
            $item = t('%langcode <a href="@url">@string</a> in context @context', $t_vars);
          }
          else {
            $item = t('%langcode <a href="@url">@string</a>', $t_vars);
          }
          $interface_list['#items'][] = array(
            '#wrapper_attributes' => array(
              'class' => array($class),
            ),
            '#markup' => $item,
          );
        }
      }
    }
  }
  else {
    $interface_list['#items'] = array(t('None.'));
  }

  // Handle recorded configuration data.
  $config_list = array(
    '#theme' => 'item_list',
    '#items' => array(),
    '#title' => t('Configuration'),
  );
  $config_recorder = \Drupal::service('language.config_factory_override');
  if ($config_recorder instanceof BabelRecorderInterface) {
    $config_data = $config_recorder->getRecordedData();
    if (!empty($config_data)) {

      /** @var \Drupal\config_translation\ConfigMapperInterface[] $mappers */
      $config_routes = array();
      $mappers = \Drupal::service('plugin.manager.config_translation.mapper')->getMappers();
      /** @var \Drupal\Core\Config\ConfigManager $config_manager */
      $config_manager = \Drupal::service('config.manager');
      foreach ($mappers as $mapper) {
        if ($names = $mapper->getConfigNames()) {
          foreach ($names as $name) {
            if (!isset($config_routes[$name])) {
              $config_routes[$name] = $mapper->getOverviewPath();
            }
          }
        }
        elseif ($mapper instanceof ConfigEntityMapper) {
          $config_routes[$mapper->getType()] = $mapper;
        }
      }

      foreach ($config_data as $langcode => $names) {
        foreach ($names as $name => $status) {
          // Skip if the langcode equals the original config langcode.
          $original = \Drupal::configFactory()->getEditable($name)->get('langcode');
          if (empty($original) || $original == $langcode) {
            continue;
          }

          $class = 'babel--' . ($status ? 'translated' : 'untranslated');
          $t_vars = array(
            '%langcode' => $langcode,
            '@name' => $name,
          );
          $item = t('%langcode @name', $t_vars);

          if (isset($config_routes[$name]) && is_string($config_routes[$name])) {
            $t_vars['@url'] = '/' . $config_routes[$name] . '/' . $langcode . '/edit';
            $item = t('%langcode <a href="@url">@name</a>', $t_vars);
          }
          else {
            $entity = \Drupal::service('config.manager')->loadConfigEntityByName($name);
            if (!empty($entity) && isset($config_routes[$entity->getEntityTypeId()])) {
              $mapper = clone $config_routes[$entity->getEntityTypeId()];
              $mapper->setEntity($entity);
              $t_vars['@url'] = '/' . $mapper->getOverviewPath() . '/' . $langcode . '/edit';
              $item = t('%langcode <a href="@url">@name</a>', $t_vars);
            }
          }
          $config_list['#items'][] = array(
            '#wrapper_attributes' => array(
              'class' => array($class),
            ),
            '#markup' => $item,
          );
        }
      }
    }
    if (empty($config_list['#items'])) {
      $config_list['#items'] = array(t('None.'));
    }
  }
  else {
    $config_list['#items'] = array(t('Unable to collect data.'));
  }

  // Handle recorded content data.
  $content_list = array(
    '#theme' => 'item_list',
    '#items' => array(),
    '#title' => t('Content'),
  );
  $fake_array = array();
  $content_info = babel_language_fallback_candidates_alter($fake_array, array(), TRUE);
  if (count($content_info)) {
    foreach ($content_info as $langcode => $entity_data) {
      foreach ($entity_data as $type => $id_data) {
        foreach ($id_data as $id => $status) {
          /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
          $entity = entity_load($type, $id);
          if (!empty($entity)) {
            $class = 'babel--' . ($status ? 'translated' : 'untranslated');
            $t_vars = array(
              '%langcode' => $langcode,
              '@name' => $entity->label(),
              '@url' => $entity->url('canonical'),
            );
            $item = t('%langcode <a href="@url">@name</a>', $t_vars);
            $content_list['#items'][] = array(
              '#wrapper_attributes' => array(
                'class' => array($class),
              ),
              '#markup' => $item,
            );
          }
        }
      }
    }
  }
  if (empty($content_list['#items'])) {
    $content_list['#items'] = array(t('None.'));
  }

  // Add to page content.
  $content = array(
    '#attached' => array('library' => array('babel/babel.display')),
    '#prefix' => '<div class="babel--container">',
    '#suffix' => '</div>',
    'lists' => array($interface_list, $config_list, $content_list),
  );
  $variables['page']['content'][] = $content;
}